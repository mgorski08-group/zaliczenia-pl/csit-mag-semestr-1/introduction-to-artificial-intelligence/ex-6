# This repository contains my attempt at the following exercise

---

## Introduction to Artificial Intelligence

## Laboratory 6

### Exercise 1

Using tensorflow, construct and train a CNN for classification of MNIST dataset. Be able to explain details of your implementation and justify the decisions made.
